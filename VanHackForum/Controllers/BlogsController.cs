﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VanHackForum.Models;
using Microsoft.AspNet.Identity;
using System.IO;
using PagedList;
using System.Text.RegularExpressions;

namespace VanHackForum.Controllers
{
    public class BlogsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Blogs
        public ActionResult Index(int? page, string tag)
        {
            int pageSize = 20;
            int pageNumber = page ?? 1;
            List<Blog> allBlogs = new List<Blog>();

            if (!string.IsNullOrWhiteSpace(tag))
            {
                ViewBag.Tag = tag;
                allBlogs = db.Blogs.Include(b => b.CreatedBy).Where(x => x.BlogTags.Contains(tag)).OrderByDescending(x => x.CreatedDateTime).ToList();
            }
            else { allBlogs = db.Blogs.Include(b => b.CreatedBy).OrderByDescending(x => x.CreatedDateTime).ToList(); }

            return View(allBlogs.ToPagedList(pageNumber, pageSize));
        }

        // GET: Blogs/Details/5
        public async Task<ActionResult> Details(String title)
        {
            if (String.IsNullOrWhiteSpace(title))
            { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            Blog blog = await db.Blogs.Include(x => x.Comments).Include(x => x.CreatedBy).Where(x => x.BlogUrl == title && x.Deleted == 0).FirstOrDefaultAsync();

            if (blog == null)
            { return HttpNotFound(); }

            if (blog.Tags != null && blog.Tags.Count > 1)
                ViewBag.RelatedBlogs = getRelatedBlogs(blog.Tags, blog);

            return View(blog);
        }

        private IEnumerable<Blog> getRelatedBlogs(IEnumerable<Tag> blogTags, Blog blog)
        {
            var blogs = new List<Blog>();

            for (int i = 0; i < blogTags.Count(); i++)
            {
                blogs = blogTags.ElementAt(i).BlogPosts.Except(new List<Blog>() { blog }).ToList();

                if (blogs.Count > 4) { break; }
            }
            return blogs.Take(4);
        }

        // GET: Blogs/Create
        [Authorize]
        public ActionResult Create()
        {
            var tagList = db.Tags.Select(x => x.BlogTag).ToList();
            string tags = "";
            tagList.ForEach((x) => tags += x + ", ");
            ViewBag.Tags = tags;
            return View(new Blog());
        }

        // POST: Blogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Create(Blog blog, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string title = getBlogTitle(blog);
                    blog.BlogUrl = title;
                    blog.CreatedById = User.Identity.GetUserId();
                    if (blog.BlogTags != null)
                    {
                        sortTags(ref blog);
                    }
                    db.Blogs.Add(blog);
                    blog.ImageName = saveBlogHeader(image);

                    var user = db.Users.Find(blog.CreatedById);
                    if (user.Blogs == null) { user.Blogs = new List<Blog>(); }
                    user.Blogs.Add(blog);

                    db.Entry(user).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Details", new { title });
                }
                catch (Exception ex)
                {
                    TempData["ErrorMessage"] = ex.Message;
                }
            }

            return View(blog);
        }

        private string getBlogTitle(Blog blog)
        {
            blog.Title = blog.Title.Trim();
            var title = Regex.Replace(blog.Title, "[+=/]", "");
            title = title.Replace(' ', '_') + "_" + getRandomNumber();
            return title;
        }

        private string getRandomNumber()
        {
            var ticksOnVals = new DateTime(2018, 2, 14).Ticks;
            var diffInTicks = DateTime.Now.Ticks - ticksOnVals;
            return diffInTicks.ToString("x");
        }

        private void sortTags(ref Blog blog)
        {
            blog.Tags = new List<Tag>();
            var tags = blog.BlogTags.Split(new string[] { ",", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var item in tags)
            {
                var newTag = item.Trim().ToLower();
                Tag tag = db.Tags.SingleOrDefault(x => x.BlogTag.ToLower() == newTag.ToLower());
                if (tag == null)
                {
                    tag = new Tag() { BlogTag = newTag, CreatedById = User.Identity.GetUserId() };
                    db.Tags.Add(tag);
                }
                blog.Tags.Add(tag);
            }
        }

        private string saveBlogHeader(HttpPostedFileBase BlogImage)
        {
            string headerImage = null;
            if (BlogImage != null && BlogImage.ContentLength > 0 && BlogImage.ContentType.Contains("image"))
            {
                if (BlogImage.ContentLength > 1048576)
                { throw new Exception("Header image too large. Please add a header image less than 1mb in size"); }
                headerImage = Guid.NewGuid().ToString("N") + Path.GetExtension(BlogImage.FileName);
                var folderPath = Server.MapPath("~/Content/Blog/Headers");
                if (Directory.Exists(folderPath)) { }
                else { Directory.CreateDirectory(folderPath); }
                BlogImage.SaveAs(Path.Combine(folderPath, headerImage));
            }
            return headerImage;
        }

        // GET: Blogs/Edit/5
        [Authorize]
        public async Task<ActionResult> Edit(string blogtitle)
        {
            try
            {
                if (blogtitle == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Blog blog = db.Blogs.SingleOrDefault(x => x.BlogUrl == blogtitle);
                if (blog == null)
                {
                    return HttpNotFound();
                }

                var userId = User.Identity.GetUserId();
                if (blog.CreatedById != userId)
                {
                    throw new Exception("Access Denied.");
                }

                var tagList = db.Tags.Select(x => x.BlogTag).ToList();
                string tags = "";
                tagList.ForEach((x) => tags += x + ", ");
                ViewBag.Tags = tags;
                return View("Create", blog);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Index");
            }
        }

        // POST: Blogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Edit(Blog newblog, HttpPostedFileBase image)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userId = User.Identity.GetUserId();
                    var blog = db.Blogs.Find(newblog.ID);
                    if (blog.CreatedById != userId)
                    {
                        throw new Exception("Access Denied.");
                    }

                    blog.Title = newblog.Title; blog.Content = newblog.Content; blog.BlogTags = newblog.BlogTags;

                    string title = getBlogTitle(blog);
                    blog.BlogUrl = title;
                    sortTags(ref blog);
                    if (image != null)
                    {
                        blog.ImageName = saveBlogHeader(image);
                    }
                    db.Entry(blog).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Details", new { title });
                }
                return View("Create", newblog);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Index");
            }
        }

        // GET: Blogs/Delete/5
        [Authorize]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = await db.Blogs.FindAsync(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // POST: Blogs/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                Blog blog = await db.Blogs.FindAsync(id);
                if (blog != null)
                {
                    var userId = User.Identity.GetUserId();
                    if (blog.CreatedById != userId) { throw new Exception("access denied"); }
                    db.Blogs.Remove(blog);
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex) { TempData["ErrorMessage"] = ex.Message; }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize]
        [ValidateInput(false)]
        public async Task<ActionResult> PostComment(string comment, long blogId)
        {
            try
            {
                Blog blog = await db.Blogs.FindAsync(blogId);
                if (blog != null)
                {
                    var userId = User.Identity.GetUserId();
                    var blogComment = new Comment() { BlogPostID = blogId, Content = comment, CreatedById = userId };
                    blog.Comments.Add(blogComment);

                    db.Comments.Add(blogComment);
                    db.Entry(blog).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Details", new { title = blog.BlogUrl });
                }
            }
            catch (Exception ex) { TempData["ErrorMessage"] = ex.Message; }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize]
        [ValidateInput(false)]
        public async Task<ActionResult> EditComment(string editComment, long blogId, long commentId)
        {
            try
            {
                Blog blog = await db.Blogs.FindAsync(blogId);
                if (blog != null)
                {
                    var userId = User.Identity.GetUserId();
                    var blogComment = db.Comments.Find(commentId);

                    if (blogComment.CreatedById != userId || blogComment.BlogPostID != blogId)
                    {
                        throw new Exception("invalid comment details");
                    }
                    blogComment.Content = editComment;
                    db.Entry(blogComment).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Details", new { title = blog.BlogUrl });
                }
            }
            catch (Exception ex) { TempData["ErrorMessage"] = ex.Message; }
            return RedirectToAction("Index");
        }

        public ActionResult Categories()
        {
            var tags = db.Tags.Select(x => x.BlogTag).ToList();
            return View(tags);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
