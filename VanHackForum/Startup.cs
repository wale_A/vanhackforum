﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VanHackForum.Startup))]
namespace VanHackForum
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
