﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VanHackForum.Models
{
    public class BasicInfo
    {
        public BasicInfo()
        {
            CreatedDateTime = DateTime.Now;
            Deleted = 0;
            Version = 0;
        }

        [Key]
        public long ID { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int Deleted { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public int Version { get; set; }
        public string CreatedById { get; set; }
        public virtual ApplicationUser CreatedBy { get; set; }
    }
    
    public class Blog : BasicInfo
    {
        public Blog()
        {
            Tags = new List<Tag>();
            Comments = new List<Comment>();
            BlogTags = "";
        }

        [Required]
        [MaxLength(300, ErrorMessage = "Title cannot be more than 300 characters.")]
        public string Title { get; set; }
        public string BlogUrl { get; set; }
        [Required, MinLength(50)]
        [StringLength(Int32.MaxValue)]
        public string Content { get; set; }
        public string ImageName { get; set; }
        public string BlogTags { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
    
    public class Draft : BasicInfo
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }

    public class Tag : BasicInfo
    {
        public Tag()
        {
            BlogPosts = new List<Blog>();
        }

        [Required]
        [Index(IsUnique = true)]
        [StringLength(150)]
        public string BlogTag { get; set; }

        public virtual ICollection<Blog> BlogPosts { get; set; }
    }

    public class Comment : BasicInfo
    {
        [Required]
        public string Content { get; set; }
        [Required]
        public long BlogPostID { get; set; }

        public virtual Blog BlogPost { get; set; }        
    }

}