﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace VanHackForum.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public virtual ICollection<Blog> Blogs { get; set; }
        public virtual ICollection<Draft> Drafts { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        { 
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public override int SaveChanges()
        {
            try
            {
                var addedEntityList = ChangeTracker.Entries()
                                    .Where(x => x.Entity is BasicInfo &&
                                    (x.State == EntityState.Added ));

                for (int i = 0; i < addedEntityList.Count(); i++)
                {
                    var obj = ((BasicInfo)addedEntityList.ElementAtOrDefault(i).Entity);

                    obj.CreatedDateTime = System.DateTime.UtcNow;
                    obj.ModifiedDateTime = System.DateTime.MinValue;
                }

                var modifiedEntityList = ChangeTracker.Entries()
                                    .Where(x => x.Entity is BasicInfo &&
                                    (x.State == EntityState.Modified));

                for (int i = 0; i < modifiedEntityList.Count(); i++)
                {
                    var obj = ((BasicInfo)modifiedEntityList.ElementAtOrDefault(i).Entity);

                    obj.Version++;
                    obj.ModifiedDateTime = System.DateTime.UtcNow;
                }

                var deletedEntityList = ChangeTracker.Entries()
                                    .Where(x => x.Entity is BasicInfo &&
                                    (x.State == EntityState.Deleted));

                for (int i = 0; i < deletedEntityList.Count(); i++)
                {
                    var obj = ((BasicInfo)deletedEntityList.ElementAtOrDefault(i).Entity);

                    obj.Deleted = System.Convert.ToInt32(obj.ID);
                    obj.ModifiedDateTime = System.DateTime.UtcNow;
                }

                return base.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors.SelectMany(x => x.ValidationErrors).Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);
                var exceptionMessge = string.Concat(ex.Message, " The validation error are: ", fullErrorMessage);

                throw new System.Data.Entity.Validation.DbEntityValidationException(exceptionMessge, ex.EntityValidationErrors);
            }
        }

        public DbSet<Comment> Comments { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Draft> BlogDrafts { get; set; }
    }
}