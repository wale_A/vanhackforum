namespace VanHackForum.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init_11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Blogs", "BlogImage_Id", c => c.Long());
            CreateIndex("dbo.Blogs", "BlogImage_Id");
            AddForeignKey("dbo.Blogs", "BlogImage_Id", "dbo.BlogImages", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Blogs", "BlogImage_Id", "dbo.BlogImages");
            DropIndex("dbo.Blogs", new[] { "BlogImage_Id" });
            DropColumn("dbo.Blogs", "BlogImage_Id");
        }
    }
}
