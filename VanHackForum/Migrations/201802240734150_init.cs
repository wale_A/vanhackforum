namespace VanHackForum.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Drafts",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Title = c.String(),
                        Text = c.String(),
                        CreatedDateTime = c.DateTime(nullable: false),
                        Deleted = c.Int(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        Version = c.Int(nullable: false),
                        CreatedById = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .Index(t => t.CreatedById);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.Blogs",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 300),
                        BlogUrl = c.String(),
                        Content = c.String(nullable: false),
                        HasImage = c.Boolean(nullable: false),
                        BlogTags = c.String(),
                        CreatedDateTime = c.DateTime(nullable: false),
                        Deleted = c.Int(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        Version = c.Int(nullable: false),
                        CreatedById = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .Index(t => t.CreatedById);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                        BlogPostID = c.Long(nullable: false),
                        CreatedDateTime = c.DateTime(nullable: false),
                        Deleted = c.Int(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        Version = c.Int(nullable: false),
                        CreatedById = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Blogs", t => t.BlogPostID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .Index(t => t.BlogPostID)
                .Index(t => t.CreatedById);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        BlogTag = c.String(nullable: false, maxLength: 150),
                        CreatedDateTime = c.DateTime(nullable: false),
                        Deleted = c.Int(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        Version = c.Int(nullable: false),
                        CreatedById = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .Index(t => t.BlogTag, unique: true)
                .Index(t => t.CreatedById);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.BlogImages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        HeaderImage = c.Binary(),
                        BlogId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.TagBlogs",
                c => new
                    {
                        Tag_ID = c.Long(nullable: false),
                        Blog_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_ID, t.Blog_ID })
                .ForeignKey("dbo.Tags", t => t.Tag_ID, cascadeDelete: true)
                .ForeignKey("dbo.Blogs", t => t.Blog_ID, cascadeDelete: true)
                .Index(t => t.Tag_ID)
                .Index(t => t.Blog_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Drafts", "CreatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Tags", "CreatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.TagBlogs", "Blog_ID", "dbo.Blogs");
            DropForeignKey("dbo.TagBlogs", "Tag_ID", "dbo.Tags");
            DropForeignKey("dbo.Blogs", "CreatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Comments", "CreatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Comments", "BlogPostID", "dbo.Blogs");
            DropIndex("dbo.TagBlogs", new[] { "Blog_ID" });
            DropIndex("dbo.TagBlogs", new[] { "Tag_ID" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.Tags", new[] { "CreatedById" });
            DropIndex("dbo.Tags", new[] { "BlogTag" });
            DropIndex("dbo.Comments", new[] { "CreatedById" });
            DropIndex("dbo.Comments", new[] { "BlogPostID" });
            DropIndex("dbo.Blogs", new[] { "CreatedById" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Drafts", new[] { "CreatedById" });
            DropTable("dbo.TagBlogs");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.BlogImages");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.Tags");
            DropTable("dbo.Comments");
            DropTable("dbo.Blogs");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Drafts");
        }
    }
}
