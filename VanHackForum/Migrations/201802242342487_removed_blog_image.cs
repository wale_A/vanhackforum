namespace VanHackForum.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removed_blog_image : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Blogs", "BlogImage_Id", "dbo.BlogImages");
            DropIndex("dbo.Blogs", new[] { "BlogImage_Id" });
            AddColumn("dbo.Blogs", "ImageName", c => c.String());
            DropColumn("dbo.Blogs", "HasImage");
            DropColumn("dbo.Blogs", "BlogImage_Id");
            DropTable("dbo.BlogImages");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.BlogImages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        HeaderImage = c.Binary(),
                        BlogId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Blogs", "BlogImage_Id", c => c.Long());
            AddColumn("dbo.Blogs", "HasImage", c => c.Boolean(nullable: false));
            DropColumn("dbo.Blogs", "ImageName");
            CreateIndex("dbo.Blogs", "BlogImage_Id");
            AddForeignKey("dbo.Blogs", "BlogImage_Id", "dbo.BlogImages", "Id");
        }
    }
}
