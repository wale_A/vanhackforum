CREATE TABLE [dbo].[Drafts] (
    [ID] [int] NOT NULL IDENTITY,
    [Title] [nvarchar](max),
    [Text] [nvarchar](max),
    [CreatedById] [nvarchar](128),
    [CreatedDateTime] [datetime] NOT NULL,
    [Deleted] [int] NOT NULL,
    [ModifiedDateTime] [datetime],
    [Version] [int] NOT NULL,
    CONSTRAINT [PK_dbo.Drafts] PRIMARY KEY ([ID])
)
CREATE INDEX [IX_CreatedById] ON [dbo].[Drafts]([CreatedById])
CREATE TABLE [dbo].[AspNetUsers] (
    [Id] [nvarchar](128) NOT NULL,
    [Email] [nvarchar](256),
    [EmailConfirmed] [bit] NOT NULL,
    [PasswordHash] [nvarchar](max),
    [SecurityStamp] [nvarchar](max),
    [PhoneNumber] [nvarchar](max),
    [PhoneNumberConfirmed] [bit] NOT NULL,
    [TwoFactorEnabled] [bit] NOT NULL,
    [LockoutEndDateUtc] [datetime],
    [LockoutEnabled] [bit] NOT NULL,
    [AccessFailedCount] [int] NOT NULL,
    [UserName] [nvarchar](256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY ([Id])
)
CREATE UNIQUE INDEX [UserNameIndex] ON [dbo].[AspNetUsers]([UserName])
CREATE TABLE [dbo].[Blogs] (
    [ID] [int] NOT NULL IDENTITY,
    [Title] [nvarchar](300) NOT NULL,
    [BlogUrl] [nvarchar](max),
    [Content] [nvarchar](max) NOT NULL,
    [HeaderImage] [varbinary](max),
    [CreatedById] [nvarchar](128),
    [ModifiedById] [nvarchar](128),
    [CreatedDateTime] [datetime] NOT NULL,
    [Deleted] [int] NOT NULL,
    [ModifiedDateTime] [datetime],
    [Version] [int] NOT NULL,
    [ApplicationUser_Id] [nvarchar](128),
    CONSTRAINT [PK_dbo.Blogs] PRIMARY KEY ([ID])
)
CREATE INDEX [IX_CreatedById] ON [dbo].[Blogs]([CreatedById])
CREATE INDEX [IX_ModifiedById] ON [dbo].[Blogs]([ModifiedById])
CREATE INDEX [IX_ApplicationUser_Id] ON [dbo].[Blogs]([ApplicationUser_Id])
CREATE TABLE [dbo].[Categories] (
    [ID] [int] NOT NULL IDENTITY,
    [Name] [nvarchar](150) NOT NULL,
    [CreatedById] [nvarchar](128),
    [CreatedDateTime] [datetime] NOT NULL,
    [Deleted] [int] NOT NULL,
    [ModifiedDateTime] [datetime],
    [Version] [int] NOT NULL,
    CONSTRAINT [PK_dbo.Categories] PRIMARY KEY ([ID])
)
CREATE UNIQUE INDEX [IX_Name] ON [dbo].[Categories]([Name])
CREATE INDEX [IX_CreatedById] ON [dbo].[Categories]([CreatedById])
CREATE TABLE [dbo].[Comments] (
    [ID] [int] NOT NULL IDENTITY,
    [Content] [nvarchar](max) NOT NULL,
    [BlogPostID] [int] NOT NULL,
    [CreatedById] [nvarchar](128),
    [CreatedDateTime] [datetime] NOT NULL,
    [Deleted] [int] NOT NULL,
    [ModifiedDateTime] [datetime],
    [Version] [int] NOT NULL,
    CONSTRAINT [PK_dbo.Comments] PRIMARY KEY ([ID])
)
CREATE INDEX [IX_BlogPostID] ON [dbo].[Comments]([BlogPostID])
CREATE INDEX [IX_CreatedById] ON [dbo].[Comments]([CreatedById])
CREATE TABLE [dbo].[AspNetUserClaims] (
    [Id] [int] NOT NULL IDENTITY,
    [UserId] [nvarchar](128) NOT NULL,
    [ClaimType] [nvarchar](max),
    [ClaimValue] [nvarchar](max),
    CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]([UserId])
CREATE TABLE [dbo].[AspNetUserLogins] (
    [LoginProvider] [nvarchar](128) NOT NULL,
    [ProviderKey] [nvarchar](128) NOT NULL,
    [UserId] [nvarchar](128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey], [UserId])
)
CREATE INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]([UserId])
CREATE TABLE [dbo].[AspNetUserRoles] (
    [UserId] [nvarchar](128) NOT NULL,
    [RoleId] [nvarchar](128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY ([UserId], [RoleId])
)
CREATE INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]([UserId])
CREATE INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]([RoleId])
CREATE TABLE [dbo].[AspNetRoles] (
    [Id] [nvarchar](128) NOT NULL,
    [Name] [nvarchar](256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY ([Id])
)
CREATE UNIQUE INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]([Name])
CREATE TABLE [dbo].[CategoryBlogs] (
    [Category_ID] [int] NOT NULL,
    [Blog_ID] [int] NOT NULL,
    CONSTRAINT [PK_dbo.CategoryBlogs] PRIMARY KEY ([Category_ID], [Blog_ID])
)
CREATE INDEX [IX_Category_ID] ON [dbo].[CategoryBlogs]([Category_ID])
CREATE INDEX [IX_Blog_ID] ON [dbo].[CategoryBlogs]([Blog_ID])
ALTER TABLE [dbo].[Drafts] ADD CONSTRAINT [FK_dbo.Drafts_dbo.AspNetUsers_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[AspNetUsers] ([Id])
ALTER TABLE [dbo].[Blogs] ADD CONSTRAINT [FK_dbo.Blogs_dbo.AspNetUsers_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[AspNetUsers] ([Id])
ALTER TABLE [dbo].[Blogs] ADD CONSTRAINT [FK_dbo.Blogs_dbo.AspNetUsers_ModifiedById] FOREIGN KEY ([ModifiedById]) REFERENCES [dbo].[AspNetUsers] ([Id])
ALTER TABLE [dbo].[Blogs] ADD CONSTRAINT [FK_dbo.Blogs_dbo.AspNetUsers_ApplicationUser_Id] FOREIGN KEY ([ApplicationUser_Id]) REFERENCES [dbo].[AspNetUsers] ([Id])
ALTER TABLE [dbo].[Categories] ADD CONSTRAINT [FK_dbo.Categories_dbo.AspNetUsers_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[AspNetUsers] ([Id])
ALTER TABLE [dbo].[Comments] ADD CONSTRAINT [FK_dbo.Comments_dbo.Blogs_BlogPostID] FOREIGN KEY ([BlogPostID]) REFERENCES [dbo].[Blogs] ([ID]) ON DELETE CASCADE
ALTER TABLE [dbo].[Comments] ADD CONSTRAINT [FK_dbo.Comments_dbo.AspNetUsers_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[AspNetUsers] ([Id])
ALTER TABLE [dbo].[AspNetUserClaims] ADD CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserLogins] ADD CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserRoles] ADD CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserRoles] ADD CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[AspNetRoles] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[CategoryBlogs] ADD CONSTRAINT [FK_dbo.CategoryBlogs_dbo.Categories_Category_ID] FOREIGN KEY ([Category_ID]) REFERENCES [dbo].[Categories] ([ID]) ON DELETE CASCADE
ALTER TABLE [dbo].[CategoryBlogs] ADD CONSTRAINT [FK_dbo.CategoryBlogs_dbo.Blogs_Blog_ID] FOREIGN KEY ([Blog_ID]) REFERENCES [dbo].[Blogs] ([ID]) ON DELETE CASCADE
CREATE TABLE [dbo].[__MigrationHistory] (
    [MigrationId] [nvarchar](150) NOT NULL,
    [ContextKey] [nvarchar](300) NOT NULL,
    [Model] [varbinary](max) NOT NULL,
    [ProductVersion] [nvarchar](32) NOT NULL,
    CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY ([MigrationId], [ContextKey])
)
INSERT [dbo].[__MigrationHistory]([MigrationId], [ContextKey], [Model], [ProductVersion])
VALUES (N'201802210601556_initial', N'VanHackForum.Migrations.Configuration',  0x1F8B0800000000000400ED1DD96E1CB9F13D40FEA1314F49A09DD161EF3A86B40B59B27685D8B2E091177933A8696AD4701FB3DD3D5E0941BE2C0FF9A4FC42C83E795537D9F77807060C4D932C168B5545B28AACFADF7FFE7BFAD393E75A5F711839817F363B9A1FCE2CECAF02DBF1D767B36DFCF0DDABD94F3FFEF94FA76F6DEFC9FA35AF7742EB91967E74367B8CE3CDEBC5225A3D620F4573CF598541143CC4F355E02D901D2C8E0F0FFFBE383A5A600262466059D6E9C7AD1F3B1E4E7E909F1781BFC29B788BDCF7818DDD28FB4E4A960954EB067938DAA0153E9BFD8AFC5FD0EACB55106EBD795A7D669DBB0E22A82CB1FB30B390EF07318A09A2AF3F4578198781BF5E6EC807E4DE3D6F30A9F780DC086703785D56D71DCBE1311DCBA26C98835A6DA338F00C011E9D64C45988CD1B917856108F90EF2D2173FC4C479D90F06C7619A2877866893DBDBE70435A4B49DE79D2E8C0628B0E0A5E202C43FF1D58175B37DE86F8CCC7DB3844EE8175BBBD779DD53FF0F35DF005FB67FED67559E4087AA48CFB403EDD86C10687F1F347FC90A17C7D39B3167CBB85D8B068C6B449C773EDC727C733EB86748EEE5D5CCC3D33F6651C84F867ECE310C5D8BE45718C433275D7364EA827F52EF475E7C42ECEBB23DC46246766BD474FEFB0BF8E1FCF66E4CF9975E53C613BFF92A1F0C97788A0914671B8C5B5BDE0A7B8F74E2E424C49F0E6F9DAAEE8EBE8F855777D5D92FFEE882AC8FB2B7F4B53560DEF12BB98C0AB9BF76A2084E19D07A70AABEAF6995018227183BE3AEB8417A1F998591FB19BD4881E9D4DAAE8E6E79B0D91AFE42BD173E1E7444C8932BC0A03EF6340BB53D7F87C87C235A6DC1454565B06DB7025607BBA28554AA5A211609AAA1CA1F978CAC76EA07CBA101E43C67DEB21C7ADE8F6F8E5F71DC86CD20B59AC1F9CD02B45ED4D40B808F9C638DFA228FA3D08ED5F50F4D8BB6A5BE2D536242CBA8C91B7E9BDB7DBC7C0C7375BEF9E72FE707D75363577BF07576845D6C5B73E6DD51ADEBB60F525D8C66FFD44B37E8A57A68AB500D0093AE7AB158EA22BC2CCD8BE08C85EB4DDAA413514FDAB03F96BBC54BC718375A4B54C6435E15522A950687F6891486BE54B89F68AE622C7D3C333AF0A239AD6A8C534AB668C6AE0796407A846362BFCCCACD0259E52A1B4E2CA35548B6D1576F962DFE5BEA08E90C2F64117D577C1DAF1F550CDABC2A8A6356A51CDAA99A24A81E9619AD584114D2AD4E299D652A1A9BDD3A28268BABDA26DF6073AA0AFBA03DDC9E1611F5B383A279FC2AA4D5C4787BCC08FB15F75983C3E7AF1C38B5727DFBFF8A18F71FE82918DC36B0FAD0B22BF717C143EEB8D753A27D8FCB0B83F2E4FE5B84CBA5D07A103A8F0ACF839D9BADC0611BF26CAA5F2AA2D57315DB6B536153978D59E222F03B714450563D42A8D0D14AC7AB7C397482B9E506CBA22974206A3C5D611F02A8BD48831E5AD16E19C354C17E2BCDD7E3106FAAA39571DBDEC652DDE1B42A7A5D9198DDD856217554185EEEF467B161DA8CF8B52298C61B526D5D757E96A61ACAED2667B6D05C972EDDE5A770FDFE0F04019B77EB07BC5B78B8AAFFD7E51D228D086B2238D67662083B0EB48DDE5F24B6D2E8939B04AF1BD2FDCEBE7D1E606C7F3BCF53C857B151298BF07E197B904F6C0D26E5C6ACD635DAD797274FF70F2EAE5F7C82667737CF272788756FF1A9492720C075A327DB4D3FEED2FB4A75F91BBEDBAAB46D290184ABB978604ECF4A52141937CFEEAD8D453B7A86F915726E0B5EAE7FC6C2A730266438B0337CCA13B1F4607341217BA3A752F2D14EAF48545CDCACAAA74404DB87E2CED9FE33B318EEB9EDB7683D376E5D6CDD84E7F2A2E6A93083BDF9FB36AE5FE5B2E9536E08A2A263BF0F3280A564E82956021658C34FC28DFFAB6556FB14969CDDA5408C9094B3AD4AF4A10399BFD4DA25F25E4C28E5E424E3DAA95504F17CC0835075E1E856AD1531C8C3A1AB8E24E420959BAB4C77770389F1F497D10A9C521E515446F86454427387E2C8BB8E3AF9C0D726BD1115A6AAA073A21451F62C925DE609FB2732D95753AE72C243216456782FAAA2393094389C77E70D2411B00C34CB921D08097405754B5081D89F43DFDE0A7361BEB7C955E97BF40D10AD9B206245AC6EE82ED00CCB5984E323D9A311D30173A5DB3C6BD51394E4383C1969D4E786E22EA0BC26608ED055178379497E04786E61B722A1B2DD25510C7641F352A03F08E9AAA3BC4388CA7BF729E556EFF96ACA3B829301AEFC8B80CC53C3261757AE66F4A8DC23EEA5BD0D08CD75C896E37EF75DDF47E22016E5AEB62295EBBD6A646A71BC05AFC146454B881EAB4402321ADA6D800B25A4D121D044063DF18C29ADD60D76500F13A7B5FE22A3EA72BFBC99ED90EC25CFC6847602E9E0CBBB19B001E1EE8CEBCF80A616A2A5078FE00A8C0CCF7370897F2141B814B7992EC9C0A4CDF9BE8CEBFF0F8646AECC9BF7A01B833757E0CC29C1CB946E04D8E1E93674D856F019AF82A47833CEBAA19EF89232BDC1BC3B3234CA5017811A6844EE76A476F2F8C983A9AE80D4BD20287B29EBCBC4FAE5F3EA96EB59281655ED328F3BA899C41812F71CC581BF27793A5878BDFDD49FB441E44E675935A4B2AB8064EF65E5782939E096B1AB34F622408A523A90E4AF16A458691DB726B4048472F1530C5F9CC006CFE78B4126CB6E731009BBDF4AC849A2A871AA0758094401861906695BBF0CE5484AFC58B52AAE5732DC6C1B39324F25A6E560658C6D8A2D6E6476C420DE6AE69053500E3B37A00B209BA053564EB33032C5317EDA9215D0A56D0A2D283A874BC483E44960E857AA8A202E435EC8323E4CBC73011B4F801746B3525C320BC203EDA938950E58ED171C8D44D9E8E0BA6AF81B3CF02819143FE042D8F82F9D8153E841E060F04FC9049A0611337B08AD78DC4C0F2DD834A80A28BD45345651D37B18FB7A28B60CB6660A936549D5329DF02D7534965A23531D2B6A292608815D8271F44E7D4C9379CF5D45199174D0C8CADA8231803011ECA07D33995B26D6F3D9114262E0323572B12F106298042D9405A1348753753A64E9D9545D7CEC20C4639005DB3484744C92F8C16E7F8A2EC7491067BCD3E9C2E80A8B0A7EFD166E3F86B264A6CF6C55AA621622FBE5B9A874EF552188B154767D1EA50F41407215A63A1946EEF6C7CE584517C8962748FE865DE0BDB93AA29AD16C0E931EF52324CC8D3981F2CF326F4EFEC592214D65561E5C95A5F9101D2AD6A3256CC4C3FD0CEA2D17A918B42C5FBDB8BC0DD7A3E7C910E6E9D05E36101649F0C602436210E44F2451F02E7B06201557AB26AE1958F4A1530A117A755708B27B02CBCE2A33E1CF9152C0B502ED5875CBC8F6501161F6538A70B811D2543A3C4F692159817242D318314AE998489A63E7359AB85004A9DC00466FC9985136501649F0C6130112925604C993E543E68280B932FD18728440665410A450658B2F13F3924D98246F0008AAA6B18E84829E227A72FA5527DC88AD89F2C68457103D80A9CC5327DA88AF0A02C6045B13EEC3256280BB2FC3A190D081903CC3460E2A430577BEA66D3DD6114B1FB5828C547837D411E3F84DB0FE41FF5E17031F658585CC178FB1FFE42A86A6FB1DF517D633B2AC659D256A9147E4B73C50237ED47B9C8AA1E52F3FBA3C75E5078677B6B31C95CF30DA4046AD98F9074B5EAB1EFE4C485187A3F5781D55EF0FE5882A7F0A8B41541F9428BB9306AC0E8C708905F32140F2A866C5F0656E218BEFC6C082B0B9D2401CBBE4F929D40FF4F73764A2F32B5632700067CDAE66212F187EDCA404A304C2ED01067D0A80AB404C33363DA09B006E4F56ACE19C93DB2768CA106D1AFA2C8AF92B250A0EBA5A3CD5CB7B3D572A6CC66A9DD62A07F90E97B6678D7A2F290C95D8CD43B4BB22DA043A3CA2A459DA7E22A055EA59469A5B59B2E00AA0340301834420EBCEEAD855C72B1C90C31D13D6C3CCDC00D2723674CDE48C7F4A8A063E55D285D520A1229C2545FAC379AEECA7B560D67DC980BC951CE76920724D7118D7555C4B9321B7C3DDB48970BC42A856ECABE14BF8BCB059963BF3E0FADE4E94FABCCAC7CE372365B3E4731F6E6B4C27CF99B7BE13AC98136AFF01EF9CE038EE2346EDAECF8F0E858C8643B9DACB28B28B2B988DD506A597EC60608EAED5092D6069D35CD7FC7A600F2BFA270F588C2BF78E8E9AF2C28D3BCADAD00292273E7F014EF5DAE7D1B3F9DCDFE95347E6D5DFFF333D3FEC0FA1012FE7B6D1D5AFF96C3E435424B0C976D93DF710741BC93D9ED2884B70A279D210A21BDEB516A9C24B589F0B40A95587210CC05E6F9479550936088668457A71BBD77CC5942956AB495342AD389B682A84819DA15BC4E4808A5046D020B4C07DA5444D5E9419BA006A6066DA2894AB7BEA9C2CE5B269F0FC8BEE993EFFCB6250577841A82D61624AB8D429237BFDFD6122EE7F16B97A4AFDD8ACE270B016135CDAA47E0DD2789F5BEA5FD862AE99E115E2C80FD46A8DF8D9042BD2A4E790D265106D37C2ACD73BC7D1B0AD27461AA5C83923C6CED730D4D41C3EC05B9C506427973606745A4D3155A4ED1E52813B0C98C5EB6E4F97C2F6F7F7879ABB91C30840DA117C9E3138118B169DA5497439B25876AB7EF97124019806B91E4A90167EC587EA4CECC59B772FAA3CE608FC9DACD731E35601E3048995CB59B7C4163AB093E8B90113669D32167B6E9AC4EC4E0DCCCAC46073D82590DBE40A047F3EA0B00727DC0275F3F515C47669B54A669AB5D6A81BBF91ED9ACEB36A98B948E7E45C827ADBC3D3D8538E4984E85875E18FC56F97D06C9AC52713BA781340D924F45EF128E103E68D8043BD36024ED491D9A892AAE3D0DC940F59954A4709355A1288DC2A11B29B97E22F70E992E6C47229B6B67071B55A5E45143559144775B91808FAED43A6447F27AF5A84506678531748801578CAF42347374F5C7135AAA6977B9417FA73A0D56D0C8BAB5E785BE7961D77368ED59A27396D07B2F3002639825149B40821245E07906A791D2860D99FDA1EA81B281657FFAC9C13A5443591A0806DE8069BF86600E2840E414372A3A117777460565492A00153460DAAEA15510F4A87DE22AC82839D7C4782DCD3A02B0DA7039B886E634E0E9F5C418AD61AAADA964D71A93BF6ADED84F83B9464F9F25C7AB16E7347B189D5FAE4B7738A5E794DFF9A46F60CF66F67D40E63C75BD02E1F745C8922694FA906AA87A3B8F36373856A7D210BB4C8F8D523FE967157075320A116CE902914097452AF0703E2128235755422E650F408A9AFA745D7AD9BAAA6704485351D577B6D7A9EC3BAB53DD3790DEA0AAEF543955769D56A9EE591D211FEAB8A6D3BA0EF53AE31DB72097C282C0D690BBD3CC5956A428D0495D06845650BA238BE1B21F25150B39B815AD95A9F65A242A93D3A26991401DB641328C310350213EEED0BBCBD1062C18CAB0E93A24D0E29BF113B23598637EADE0223C7637BCCE72AD7536AFBD0EBBC3D46A5D0D588B33C64EA636F5C10251823A4F9E36F981F79720ADF1D0C14D291027B37BA2F4970FAD3151B81319939FA7FBC1F797EEAC138EE08E0A40A8CBEE89D25776B34E48C21E26D4311EDB13A4E76C664AF455A80F420483EC657220B1D3C5C7AD4F9F0FA6BF2E71E4AC4B10344A9A8F579CD9A6A873ED3F04B9F548C028AF223E61C431B2518CCEC3D87940AB9814D3C0268E4FD68AE4ED190DAF738FED6BFFC336DE6C633264ECDDBBDC86975AA1AAFA4F52B4F1389F7ED8D05F51174320683AF4C5E507FFCDD671ED02EF2BC55D7E0004356F65AF12E95CC6F475E2FAB9807413F89A8032F21556B93BEC6D5C022CFAE02FD157DC0437C27EEFF01AAD9ECB576C1090FA89E0C97E7AE9A07588BC288351B6273F090FDBDED38FFF0777858626FDC50000 , N'6.1.3-40302')